from .api import Metax
from .types import *
from .errors import *

__author__ = 'Mikhayil Martirosyan (mikhail.martirosyan@realschool.am)'
__version__ = 'V 2.0'
