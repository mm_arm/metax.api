INT=python3
FILE=test.py
ARGS=

run:$(FILE)
	@$(INT) $(ARGS) $(FILE)

clean:
	@rm -rf metax/__pycache__/ ./__pycache__/
